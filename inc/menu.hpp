#ifndef MENU_HPP
#define MENU_HPP

#include <string>

using namespace std;

class Menu{

private:

	int opcao;
	
public:

	Menu();
	~Menu();

	void set_process();
	void set_menu_cadastro_socio();
	void set_menu_estoque();
	void set_menu_vendas();
	void set_menu_vendas(string nome, string cpf);
	void set_menu_recomendacao();
	void set_option();
	void set_nenhuma_cat();
	void set_option(int opcao);


};

#endif