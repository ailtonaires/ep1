#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include <string>
#include <vector>
#include "produto.hpp"

using namespace std;

class Estoque {

private: 

	string nome_categoria;
	string nome_produto;
	int quantidade;
	float valor;
	//vector <Produto> decrementa;


public:

	Estoque();
	Estoque(string nome_categoria, string nome_produto, int quantidade, float valor);
	Estoque( string nome_produto, int quantidade, float valor);
	~Estoque();

	void set_novo_produto(string nome_categoria, string produto, int qtd, float valor);
	void set_nova_categoria();
	void set_categoria_geral();
	string get_categoria_geral();
	void set_produto_geral(string nome);
	void set_decrementa_produto(string nome_categoria, string nome_produto, int quantidade, float valor);


};

#endif