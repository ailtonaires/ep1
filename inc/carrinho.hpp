#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <string>
#include <vector>
#include "produto.hpp"

using namespace std;

class Carrinho : private Produto{

private:

string categ;
string nome_prod;
int qtd;
int qtd_comprou;
float preco;
vector <Produto> carrinho;
vector <int> quantidade;

	
public:

	Carrinho();
	~Carrinho();

	void set_add_produto();
	vector <Produto> get_add_produto();
	void set_finalizar_pedido(vector <Produto> carrinho, vector <int> quantidade, string cpf );
	void set_finalizar_pedido(string cpf);
};

#endif