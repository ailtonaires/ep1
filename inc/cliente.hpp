#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <string>
#include <vector>
#include "pessoa.hpp"

using namespace std;

class Cliente : public Pessoa{

private:

	string nome;
	string cpf;
	bool socio_s = 0;


public:

	Cliente();
	Cliente(string nome, string cpf);
	~Cliente();

	void set_cadastro();
	void set_cadastro_socio(string nome, string cpf);
	void set_verif_socio(string cpf);
	bool get_verif_socio();
	void set_cadastro(string cpf, string nome );
	void set_add_categoria(string nome_categoria);
	void set_add_categoria(string nome_categoria, string cpf);

};

#endif