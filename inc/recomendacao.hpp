#ifndef RECOMENDACAO_HPP
#define RECOMENDACAO_HPP
#include "cliente.hpp"

#include <string>

using namespace std;

class Recomendacao{

private:

vector <string> aux_categoria;

public:

	Recomendacao();
	~Recomendacao();
	void set_recomenda(string nome, string cpf);
	bool set_maior_menor(int A, int B);


};

#endif