#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include "estoque.hpp"
#include <string>

using namespace std;

class Produto{
	
public:

	string nome_categoria;
	string nome_produto;
	int quantidade;
	float preco;

	Produto();
	Produto(string nome_produto, int quantidade, float preco );
	Produto(string nome_categoria, string nome_produto, int quantidade, float preco);
	~Produto();
	void set_novo_produto();


};

#endif