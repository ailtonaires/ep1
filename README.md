# EP1 - OO 2019.2 (UnB - Gama)

## Descrição

O projeto visa a criação de um "E-commerce" e que facilite as vendas de uma determinada loja. A aplicação tem por objetivo a utilização da POO como principal característica.

### Modo venda

No modo venda é necessário que haja antes de mais nada uma verificação da existência do cadastro do cliente, para poder prosseguir com às compras. Caso não existia, aparecerá uma opção para saber se deseja ou não fazer o cadastro. Na verificação também é necessário que seja inserido os dados pertinentes ao cliente. Após inserir a opção para fazer compras, será apresentado uma lista de categorias a qual deve ser inserida para poder ver os produtos respectivos daquela categoria. Depois de selecionar os produtos e suas quantidades, a compra será finalizada e o desconto será exibido (caso seja um sócio) juntamente com as informações pertinentes.

### Modo estoque

No modo estoque, existe a opção para criar uma nova categoria ou cadastrar produtos em categorias já existentes. Será solicitado as informações de cada produto, bem como seu nome, quantidade e valor por unidade. Após isso o cadastro do produto será efetuado.

### Modo recomendação
	
## Orientações

As orientações já foram inseridas anteriormente, porém cabe ressaltar a importância de seguir criteriosamente ao que se pede em cada entrada de dados.

IMPORTANTE: Antes de fazer o cadastro de categorias e produtos, é condição necessária que todos os Clientes já estejam cadastrados para a recomendação de produtos funcionar com exatidão. Essa condição se encontra por conta de erros constantes de segmentação em uma implementação que deveria funcionar.
