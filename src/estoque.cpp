#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "estoque.hpp"
#include "produto.hpp"
#include "menu.hpp"
#include "carrinho.hpp"
#include "cliente.hpp"


Estoque::Estoque(){

}


Estoque::Estoque(string nome_categoria, string nome_produto, int quantidade, float valor){

	this -> nome_produto = nome_produto;
	this -> quantidade = quantidade;
	this -> valor = valor;
	this -> nome_categoria = nome_categoria;

}

Estoque::Estoque( string nome_produto, int quantidade, float valor){

	this -> nome_produto = nome_produto;
	this -> quantidade = quantidade;
	this -> valor = valor;
}

Estoque::~Estoque(){

}

void Estoque::set_novo_produto(string nome_categoria, string nome_produto, int quantidade, float valor){

	this -> nome_categoria = nome_categoria;
	this -> nome_produto = nome_produto;
	this -> quantidade = quantidade;
	this -> valor = valor;

	system("clear");

	Menu escolha;
	string produto_2, ult_produto;
	int qtd_2;
	float preco_2;
	vector <Estoque> verif_produto;
	int i=0, j=0,k=0, tam, cont=0;

	fstream categoria;

	system("clear");

	while(nome_categoria[k]!='\0'){

		nome_categoria[k] = toupper(nome_categoria[k]);
		k++;
	}


	categoria.open("doc/categoria/"+nome_categoria+".txt", ios::in);
	categoria.close();

	if (categoria.fail() == 1){

		system("clear");

		cout<<"Essa categoria não existe. Deseja criar uma nova categoria?" << endl << endl;

		escolha.set_option();

	} else{

		categoria.open("doc/categoria/"+nome_categoria+".txt", ios::in);

		if (categoria.is_open() && categoria.good()){ 

			while(nome_produto[j]!='\0'){

				nome_produto[j] = toupper(nome_produto[j]);	
				j++;
			}

			while(!categoria.fail()){

				categoria >> produto_2 >> qtd_2 >> preco_2;

				verif_produto.push_back(Estoque(produto_2, qtd_2, preco_2));

				
				if(nome_produto == verif_produto[i].nome_produto){

					verif_produto[i].quantidade += quantidade;
					verif_produto[i].valor = valor;

					cont++;

				}

				i++;

			}

			categoria.close();
		}


		categoria.open("doc/categoria/"+nome_categoria+".txt", ios::out | ios::trunc);


		i=0;

		tam = verif_produto.size()-1;


		while (tam != 0){

			categoria << verif_produto[i].nome_produto << " " << verif_produto[i].quantidade << " " << verif_produto[i].valor << endl; 
			tam--;
			i++;
		}

		if (cont==0){

			categoria << nome_produto << " " << quantidade << " " << valor <<endl;

		}

		categoria.close();

		escolha.set_process();


	}


}

void Estoque::set_nova_categoria(){

	int opcao;
	Menu escolha;
	int i=0;
	Cliente add_categoria;

	system("clear");

	cout<<"Informe o nome da categoria que deseja criar"<<endl<<endl;
	cout<<"Categoria: ";
	cin>>nome_categoria;

	while(nome_categoria[i]!='\0'){

		nome_categoria[i] = toupper(nome_categoria[i]);
		i++;
	}

	ifstream category;

	category.open("doc/categoria/"+nome_categoria+".txt", ios::in);
	category.close();

	if (category.fail()==1){

		while(nome_categoria[i]!='\0'){

			nome_categoria[i] = toupper(nome_categoria[i]);
			i++;
		}

		ofstream categoria ("doc/categoria/"+nome_categoria+".txt", ios::app);
		category.close();

		fstream lista_categ ("doc/categoria/categorias_gerais.txt", ios::app);

		lista_categ << nome_categoria << endl;

		lista_categ.close();

		cout<<"Categoria criada com sucesso!"<<endl<<endl;

		add_categoria.set_add_categoria(nome_categoria);

		escolha.set_process();


	} else{

		system("clear");

		cout<<"Esta categoria já existe. Escolha uma das opções a seguir:"<<endl<<endl;

		cout<<"1) Menu Principal\n2) Novo Produto\n3) Sair"<<endl<<endl;
		cout<<"Opção: ";
		cin>>opcao;
		escolha.set_option(opcao);

	}

}



string Estoque::get_categoria_geral(){

	this -> nome_categoria = nome_categoria;

	return nome_categoria;
}

void Estoque::set_decrementa_produto(string nome_categoria, string nome_produto, int quantidade, float valor){

	this -> nome_categoria = nome_categoria;
	this -> nome_produto = nome_produto;
	this -> quantidade = quantidade;
	this -> valor = valor;
	
	

	system("clear");

	Menu escolha;
	string produto_2, ult_produto;
	int qtd_2;
	float preco_2;
	vector <Estoque> verif_produto;
	int i=0, tam, cont=0;

	fstream categoria;

	system("clear");

	categoria.open("doc/categoria/"+nome_categoria+".txt", ios::in);
	categoria.close();

	if (categoria.fail() == 1){

		system("clear");

		cout<<"ERRO!" << endl << endl;

		escolha.set_option();

	} else{

		categoria.open("doc/categoria/"+nome_categoria+".txt", ios::in);

		if (categoria.is_open() && categoria.good()){ 


			while(!categoria.fail()){

				categoria >> produto_2 >> qtd_2 >> preco_2;

				verif_produto.push_back(Estoque(produto_2, qtd_2, preco_2));

				
				if(produto_2 == nome_produto){

					verif_produto[i].quantidade = verif_produto[i].quantidade - quantidade;

					cont++;

				} 

				if (produto_2 == ult_produto){
					break;
				}

				ult_produto = produto_2;

				i++;


			}

			categoria.close();
		}


		categoria.open("doc/categoria/"+nome_categoria+".txt", ios::out | ios::trunc);


		i=0;

		tam = verif_produto.size()-1;


		while (tam != 0){

			categoria << verif_produto[i].nome_produto << " " << verif_produto[i].quantidade << " " << verif_produto[i].valor << endl; 
			tam--;
			i++;
		}


		categoria.close();


	}


}