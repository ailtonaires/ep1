#include <string>
#include <iostream>
#include <fstream>
#include "menu.hpp"
#include "produto.hpp"
#include "estoque.hpp"
#include "cliente.hpp"
#include "vendas.hpp"
#include "carrinho.hpp"
#include "recomendacao.hpp"

using namespace std;

Menu::Menu(){


}

Menu::~Menu(){


}

void Menu::set_process(){

	this-> opcao = opcao;
	Cliente cliente;

	cout<<"Escolha uma das opções a seguir:"<<endl<<endl;
	cout<<"1) Estoque\n2) Recomendação\n3) Vendas\n4) Cadastro de Cliente\n5) Cadastro de Sócio\n6) Sair"<<endl<<endl;
	cout<<"Opção: ";

	cin>>opcao;

	switch (opcao){

		case 1:

		set_menu_estoque();

		break;

		case 2:

		set_menu_recomendacao();

		break;

		case 3:

		set_menu_vendas();

		break;

		case 4:

		system("clear");
		cliente.set_cadastro();

		break;

		case 5:

		set_menu_cadastro_socio();

		break;

		case 6:

		exit(0);

		break;
	}

}

void Menu::set_menu_estoque(){

	this -> opcao = opcao;
	Produto item;
	Estoque categoria;

	system("clear");

	cout<<"Escolha uma das opções a seguir:"<<endl<<endl;
	cout<<"1) Novo produto\n2) Nova categoria"<<endl<<endl;
	cout<<"Opção: ";

	cin>>opcao;

	switch (opcao){

		case 1:

		item.set_novo_produto();

		break;

		case 2:

		categoria.set_nova_categoria();

		break;

	}



}

void Menu::set_menu_recomendacao(){

	system ("clear");

	this -> opcao = opcao; 
	string nome;
	string cpf;
	int i=0;
	Recomendacao recomendacao;


	cout << "Informe seu nome:";
	cin >> nome;

	cout << "Informe seu CPF:";
	cin >> cpf;

	while(nome[i]!='\0'){

		nome[i] = toupper(nome[i]);	
		i++;
	}


	recomendacao.set_recomenda(nome, cpf);

}

void Menu::set_menu_vendas(){

	this -> opcao = opcao; 

	string nome, cpf;
	Cliente cliente;

	system("clear");

	cout << "Você possui cadastro em nossa loja?" << endl << endl;
	cout << "1) Sim\n2) Não - Fazer cadastro" << endl << endl;
	cout << "Opção: ";

	cin>>opcao;

	system("clear");

	switch (opcao){

		case 1:

		cout << "Informe seu primeiro nome: ";
		cin >> nome;

		cout << "Informe seu CPF (somente números): ";
		cin >> cpf;

		cliente.set_cadastro (cpf, nome);

		break;

		case 2:

		cliente.set_cadastro();
		

		break;

	}


}

void Menu::set_menu_vendas(string nome, string cpf){

	system("clear");

	this -> opcao = opcao;

	Carrinho pedido;
	Vendas venda;

	cout << "Olá, " << nome << ". Bem-vindo!" << endl << endl;

	cout << "1) Fazer compras" << endl;
	cout << "2) Sair" << endl << endl;

	cout << "Opção:";
	cin >> opcao;

	switch (opcao){


		case 1:

		system("clear");

		pedido.set_finalizar_pedido(cpf);
		
		break;

		case 2:

		exit(0);
		
		break;
	}




}

void Menu::set_option(){

	this -> opcao = opcao;
	Estoque categoria;

	cout<<"1) Sim\n2) Não - Menu Principal"<<endl<<endl;
	cout<<"Opção: ";
	cin>>opcao;

	switch (opcao){

		case 1:

		categoria.set_nova_categoria();

		break;

		case 2:

		system("clear");
		set_process();

		break;

	}
}

void Menu::set_nenhuma_cat(){

	this -> opcao = opcao;

	cout<<"1)Cadastrar categoria\n2) Menu Principal\n3) Sair"<<endl<<endl;
	cout<<"Opção: ";
	cin>>opcao;

	Estoque categoria;

	switch (opcao){

		case 1:

		categoria.set_nova_categoria();

		break;

		case 2:

		system("clear");
		set_process();

		break;

		case 3:

		exit(0);

		break;

	}


}

void Menu::set_option(int opcao){

	this -> opcao = opcao;
	Produto escolha;

	switch (opcao){

		case 1:

		system("clear");
		set_process();

		break;

		case 2:

		escolha.set_novo_produto();

		break;

		case 3:

		exit(0);

		break;

	}
}


void Menu::set_menu_cadastro_socio(){

	system("clear");

	Cliente cliente;

	string nome, cpf;

	cout << "Informe seu primeiro nome: ";
	cin >> nome;

	cout << "Informe seu CPF (somente números): ";
	cin >> cpf;

	cliente.set_cadastro_socio(nome, cpf);

	system("clear");
	set_process();



}