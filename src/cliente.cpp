#include <string>
#include <iostream>
#include <fstream>
#include "cliente.hpp"
#include "pessoa.hpp"
#include "menu.hpp"
#include <ctype.h>
#include <vector>



using namespace std;

Cliente::Cliente(){


}

Cliente::Cliente(string nome, string cpf){

	this -> nome = nome;
	this -> cpf = cpf;

}


Cliente::~Cliente(){


}

void Cliente::set_cadastro_socio(string nome, string cpf){

	this -> cpf = cpf;
	this -> socio_s = socio_s;
	this -> nome = nome;

	fstream socio;
	Menu menu;
	string verif_nome, verif_cpf, verif_cpf_2;
	int i=0;

	while(nome[i]!='\0'){

		nome[i] = toupper(nome[i]);	
		i++;
	}

	socio.open("doc/socios.txt", ios::in);
	socio.close();

	if (socio.fail() == 0){

		socio.open("doc/socios.txt", ios::in);

		if (socio.is_open() && socio.good()){ 

			while(!socio.fail()){

				socio >> verif_nome >> verif_cpf;

				if (verif_cpf == cpf && verif_nome == nome){

					socio.close();

					system("clear");

					cout << "Você já é um Sócio" << endl << endl;

					menu.set_process();

				}

				if(verif_cpf == verif_cpf_2)
					break;

				verif_cpf_2 = verif_cpf;
			}

			socio.close();
		}
	}


	socio.open("doc/socios.txt", ios::app);


	if (socio.is_open() && socio.good()){ 


		socio << nome << " " << cpf <<endl;

	}else{

		cout << "ERRO INESPERADO!" <<endl;
		exit (0);

	}

} 


void Cliente::set_verif_socio(string cpf){

	this -> cpf = cpf;
	this -> socio_s = socio_s;

	fstream socio;
	string verif_nome, verif_cpf;
	

	socio.open("doc/socios.txt", ios::in);
	socio.close();

	if (socio.fail() == 0){

		socio.open("doc/socios.txt", ios::in);

		while(!socio.fail()){

			socio >> verif_nome >> verif_cpf;

			if (verif_cpf == cpf){

				socio_s = 1;

				break;
			}


		}

		socio.close();

	} else{

		cout <<"ERRO INESPERADO!" << endl;
		exit(0);
	}

	

}

bool Cliente::get_verif_socio(){

	return socio_s;

}


void Cliente::set_cadastro(){

	string nome;
	string cpf;
	Menu menu;
	fstream dados_cliente;
	string verif_nome, verif_cpf, verif_cpf_2;

	cout << "Nos informe seu nome: ";
	cin >> nome;

	cout << "\n\nNos informe seu CPF: ";
	cin >> cpf;


	int i=0;

	dados_cliente.open("doc/clientes/dados_cliente.txt", ios::in);

	while(nome[i]!='\0'){

		nome[i] = toupper(nome[i]);	
		i++;
	}

	if (dados_cliente.is_open() && dados_cliente.good()){ 

		while(!dados_cliente.fail()){

			dados_cliente >> verif_nome >> verif_cpf;

			if (verif_cpf == cpf && verif_nome == nome){

				system("clear");

				cout << "Você já possui cadastro em nossa loja." << endl << endl;

				menu.set_process();

			}	

			if(verif_cpf == verif_cpf_2)
				break;

			verif_cpf_2 = verif_cpf;
		}

		dados_cliente.close();
	}


	dados_cliente.open("doc/clientes/dados_cliente.txt", ios::app);

	dados_cliente << nome << " " << cpf <<endl;

	dados_cliente.close();

	menu.set_menu_vendas(nome, cpf);


}


void Cliente::set_cadastro(string cpf, string nome ){

	this -> cpf = cpf;
	this -> nome = nome;
	string verif_nome, verif_cpf, verif_cpf_2;
	int i=0, opcao, tem_cadastro = 0;
	Menu menu;
	fstream dados_cliente;

	dados_cliente.open("doc/clientes/dados_cliente.txt", ios::in);
	dados_cliente.close();

	if (dados_cliente.fail() == 1){

		dados_cliente.open("doc/dados_cliente.txt", ios::app);

		dados_cliente.close();


	} 

	if (dados_cliente.fail() == 0){

		dados_cliente.open("doc/clientes/dados_cliente.txt", ios::in);

		while(nome[i]!='\0'){

			nome[i] = toupper(nome[i]);	
			i++;
		}

		if (dados_cliente.is_open() && dados_cliente.good()){ 

			while(!dados_cliente.fail()){

				dados_cliente >> verif_nome >> verif_cpf;

				if (verif_cpf == cpf && verif_nome == nome){

					tem_cadastro = 1;
					break;

				}

				if(verif_cpf == verif_cpf_2)
					break;

				verif_cpf_2 = verif_cpf;
			}

			if (tem_cadastro == 0){

				system("clear");

				cout << "Você não está cadastrado no nosso banco de clientes.\nVocê deseja fazer o cadastro?" << endl << endl;

				cout << "1) Sim\n";
				cout << "2) Não - Voltar ao menu inicial" << endl << endl;

				cout << "Opção: ";
				cin >> opcao;

				system("clear");

				switch (opcao){

					case 1:

					set_cadastro();

					break;

					case 2:

					menu.set_process();

					break;

				}

			} else{

				menu.set_menu_vendas(nome, cpf);

			}



		}

		dados_cliente.close();

	}

}



void Cliente::set_add_categoria(string nome_categoria){


	this -> nome = nome;
	this -> cpf = cpf;
	string cliente_1, cpf_1, ult_cliente = "";
	vector <Cliente> dados_cliente;
	int tam, i;

	fstream dados;
	fstream dados_clientes;

	dados.open("doc/clientes/dados_cliente.txt", ios::in);

	if (dados.is_open() && dados.good()){ 

		while(!dados.fail()){

			dados >> cliente_1 >> cpf_1;

			if (cpf_1 == ult_cliente){

				break;
			}

			dados_cliente.push_back(Cliente(cliente_1, cpf_1 )); 

			ult_cliente = cpf_1;

		}

		dados.close();
	}

	tam = dados_cliente.size();

	for (i=0; i<tam; i++){

		
		dados_clientes.open("doc/clientes/" + dados_cliente[i].nome + " " + dados_cliente[i].cpf + ".txt", ios::app);

		dados_clientes << nome_categoria << " 0" << endl;

		dados_clientes.close();
		

	}

}

void Cliente::set_add_categoria(string nome_categoria, string cpf){

	this -> nome = nome;
	this -> cpf = cpf;

	string cliente_1, cpf_1, ult_cliente = "";
	string categoria, categoria_2;
	vector <string> aux_categoria;
	vector <int> aux_peso;
	int tam, i, peso, cont=0;

	fstream dados;
	fstream dados_cliente;

	dados.open("doc/clientes/dados_cliente.txt", ios::in);

	if (dados.is_open() && dados.good()){ 

		while(!dados.fail()){

			dados >> cliente_1 >> cpf_1;

			if (cpf_1 == cpf){

				break;
			}

		}

		dados.close();
	}


	dados_cliente.open("doc/clientes/" +  cliente_1 + " " + cpf + ".txt", ios::in);
	dados_cliente.close();

	if (dados_cliente.fail() == 0){

		dados_cliente.open("doc/clientes/" +  cliente_1 + " " + cpf + ".txt", ios::in);

		if (dados_cliente.is_open() && dados_cliente.good()){ 

			while(!dados_cliente.fail()){

				dados_cliente >> categoria >> peso;

				if (categoria == categoria_2)
					break;


				aux_categoria.push_back(categoria);
				aux_peso.push_back(peso);

				if (categoria == nome_categoria){

					aux_peso[cont] += 1; 

				}

				categoria_2 = categoria;

				cont++;

			}

			dados_cliente.close();
		}


	} else{

		cout << "ERRO!"<<endl;
	}

	dados_cliente.open("doc/clientes/" +  cliente_1 + " " + cpf + ".txt", ios::out | ios::trunc);

	if (dados_cliente.is_open() && dados_cliente.good()){ 

		tam = aux_peso.size();

		for(i=0; i<tam; i++){

			dados_cliente << aux_categoria[i] << " " << aux_peso[i] <<endl;

		}

	}

	dados_cliente.close();



}

