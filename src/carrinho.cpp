#include <string>
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <vector>
#include "produto.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "menu.hpp"
#include "cliente.hpp"


using namespace std;

Carrinho::Carrinho(){


}

Carrinho::~Carrinho(){


}

void Carrinho::set_add_produto(){

	this -> categ = categ;
	this -> nome_prod = nome_prod;
	this -> qtd = qtd;
	this -> qtd_comprou = qtd_comprou;
	this -> preco = preco;
	this -> carrinho = carrinho;

	carrinho.push_back(Produto(categ, nome_prod, qtd_comprou, preco));


}

vector <Produto> Carrinho::get_add_produto(){

	this -> carrinho = carrinho;

	return carrinho;


}

void Carrinho::set_finalizar_pedido(vector <Produto> carrinho, vector <int> quantidade, string cpf ){

	system("clear");


	this -> carrinho = carrinho;
	this -> quantidade = quantidade;


	Estoque estoque;
	Produto produto;
	Menu menu;
	string verif_nome;
	string verif_cpf;
	Cliente cliente_socio;
	Cliente recomendacao;
	int tam = carrinho.size();
	int i=0, opcao;
	float valor_total = 0.0;
	float desconto = 0;
	float valor_recebido;




	cout << "CARRINHO"<<endl<<endl;

	for (i=0; i<tam; i++){

		cout << carrinho[i].nome_categoria << " -- " << carrinho[i].nome_produto << " -- " << quantidade[i] << " -- " << carrinho[i].preco << endl;

		recomendacao.set_add_categoria(carrinho[i].nome_categoria, cpf);

		valor_total = valor_total + (quantidade[i] * carrinho[i].preco);

	}

	cliente_socio.set_verif_socio(cpf);

	if (cliente_socio.get_verif_socio()==1){

		desconto = (valor_total*0.15);

	}

	cout << fixed;
	cout.precision(2);

	cout << "\nVALOR TOTAL -> R$: " << valor_total << endl;
	cout << "DESCONTO: -> R$: " << desconto << endl<<endl;
	cout << "VALOR FINAL: -> R$: " << (valor_total-desconto) << endl;
	cout << "VALOR RECEBIDO: -> R$: ";
	do{

		cin >> valor_recebido;

	}while (valor_recebido < (valor_total-desconto));

	cout << "\nTROCO: -> R$: " << (valor_recebido - (valor_total - desconto));



	cout << "\n\nDeseja finalizar o pedido?" << endl << endl;
	cout << "1) Sim\n2) Não" << endl << endl;

	cin >> opcao;

	switch (opcao){

		case 1:

		for (i=0; i<tam; i++) {

			estoque.set_decrementa_produto(carrinho[i].nome_categoria, carrinho[i].nome_produto, quantidade[i], carrinho[i].preco);

			cout << "OBRIGADO PELA PREFERÊNCIA!!!" << endl << endl;
			exit(0);
		}


		break;

		case 2:
		system("clear");
		menu.set_process();

		break;
	}


	exit(0);
}

void Carrinho::set_finalizar_pedido(string cpf){


	this -> nome_categoria = nome_categoria;
	this -> qtd_comprou = qtd_comprou;

	ifstream lista_categ;
	string print_categ, ver_categ;
	Menu menu;
	vector <string> name_categ;
	vector <Produto> compras;
	fstream all_produtos;
	string lista_produtos;
	string print_produtos;
	Carrinho carrinho;
	int print_quantidade;
	float print_valor;

	string ver_produtos;
	vector <Produto> name_produtos;
	int i=1;

	do{

		system("clear");

		name_produtos.clear();

		lista_categ.open("doc/categoria/categorias_gerais.txt", ios::in);
		lista_categ.close();

		if(lista_categ.fail() == 1){

			cout << "Nenhuma categoria cadastrada até o momento."<<endl<<endl;

			menu.set_nenhuma_cat();


		} 

		if (lista_categ.fail() == 0){ 

			lista_categ.open("doc/categoria/categorias_gerais.txt", ios::in);

			i=1;

			cout << "Lista de categorias" << endl << endl;

			while(!lista_categ.fail()){

				lista_categ >> print_categ;
				name_categ.push_back(print_categ);

				if (print_categ == ver_categ)
					break;

				cout << i << ") " << print_categ << endl;

				ver_categ = print_categ;
				i++;

			} 

			lista_categ.close();

			cout << "	\n\nDigite 0 para finalizar a compra" <<endl <<endl;
			cout << "Você deseja consultar os produtos de qual categoria?" << endl << endl;
			cout << "Opção:";
			cin >> i;

			if(i==0){
				carrinho.set_finalizar_pedido(compras,quantidade, cpf);
			}

			nome_categoria = name_categ[i-1];

			all_produtos.open("doc/categoria/"+nome_categoria+".txt", ios::in);
			all_produtos.close();

			i=0;

			if (all_produtos.fail() == 0){

				system("clear");

				cout << "Lista de produtos"<<endl<<endl;

				all_produtos.open("doc/categoria/"+nome_categoria+".txt", ios::in);

				while(!all_produtos.fail()){

					all_produtos >> print_produtos >> print_quantidade >> print_valor;
					name_produtos.push_back(Produto(nome_categoria, print_produtos, print_quantidade, print_valor));

					if (ver_produtos == print_produtos)
						break;

					cout << i+1 << ") " << name_produtos[i].nome_produto << " - R$: " << name_produtos[i].preco << " ---- Quantidade em Estoque: " << name_produtos[i].quantidade << endl;

					ver_produtos = print_produtos;
					i++;

				} 

				all_produtos.close();


				cout << "Qual produto deseja adicionar ao carrinho:" << endl << endl;
				cout << "Opção:";
				cin >> i;

				cout << "Quantidade:";
				cin >> qtd_comprou;
				quantidade.push_back(qtd_comprou);

				if ((name_produtos[i-1].quantidade - qtd_comprou)>=0 ){	

					compras.push_back(Produto(nome_categoria, name_produtos[i-1].nome_produto, name_produtos[i-1].quantidade, name_produtos[i-1].preco));

					system("clear");

				} else{

					system("clear");

					cout << "Não temos o produto " << "\""<< name_produtos[i-1].nome_produto << "\"" << " ou a quantidade no estoque é insuficiente!" << endl << endl;


				}	


			}else{

				cout << "Não foi possível executar a ação. Erro inesperado!" << endl;
				exit(0);	

			}
		}

	} while (i!=0);

}
